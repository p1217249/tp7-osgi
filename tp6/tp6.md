﻿# TP6 - OSGi

## Objectifs pédagogiques

Expérimenter une architecture à base de composants téléchargeables dynamiquement.

## Outils

Dans ce TP, vous utiliserez le framework [Felix](http://felix.apache.org/documentation/subprojects/apache-felix-ipojo.html), qui contient une implémentation minimale d'OSGi et une console ("gogo" shell) en ligne de commande. Un début de documentation de ce shell est disponible [ici](https://felix.apache.org/documentation/subprojects/apache-felix-framework/apache-felix-framework-usage-documentation.html#framework-shell).

## Découverte du framework Felix

Téléchargez et décompressez le framework. Vous pouvez ouvrir un shell en exécutant, depuis le répertoire principal du framework, la commande `java -jar bin/felix.jar`.

Utiliser la documentation située [ici](https://felix.apache.org/documentation/subprojects/apache-felix-framework/apache-felix-framework-usage-documentation.html#framework-shell) pour démarrer. Par exemple, taper "lb" (list bundles) pour avoir la liste des bundles installés par défaut.

### Chargement et exécution d'un bundle

Explorer le [Oscar Bundle Repository](http://oscar-osgi.sourceforge.net/). Choisir et télécharger un ou plusieurs bundle(s), installer, lancer et stopper ce(s) bundle(s) comme indiqué [là](http://felix.apache.org/site/apache-felix-framework-usage-documentation.html#ApacheFelixFrameworkUsageDocumentation-installingbundles). Normalement, vous devez voir s'exécuter les méthodes de gestion du cycle de vie start() et stop(), d'implémentation de l'interface BundleActivator.

Dans la suite, vous pourrez avoir besoin de recourir à ce repo pour récupérer et déployer des bundles nécessaires à l'exécution de votre application.

### Réalisation d'un bundle avec iPOJO

Vous allez maintenant réaliser vous-même un bundle OSGi, en suivant [ce tutoriel](http://felix.apache.org/documentation/subprojects/apache-felix-ipojo/apache-felix-ipojo-gettingstarted/ipojo-hello-word-maven-based-tutorial.html). Une fois les fichiers recopiés et les jars faits, suivre la même procédure que précédemment pour les déployer et les démarrer. Vous devrez démarrer les bundles suivants (dans "annotations/hello.felix.annotations/target/bundle") : `org.apache.felix.ipojo-1.12.1`, `hello.service-1.12.1`, `hello.impl.annotations-1.12.1` et `hello.client.annotations-1.12.1`.

## Reprise de l'application entrepôt

### Première approximation

De la même manière que dans les TP précédents, vous allez reprendre l'application de gestion des entrepôts et l'instancier dans le framework [OSGi](https://spring.io/projects).

On pourra pour cela soit reprendre le travail réalisé pour le TP1, soit utiliser le code de correction partielle du TP1 fourni dans la branche `correction` du dépôt.

**Pour commencer, réalisez l'opération suivante sur un seul objet métier : `EntrepotOperations`.**

Créer trois bundles contenant :

  * l'interface standardisée d'un entrepôt
  * l'entrepôt et son contenu (Entrepot, Marchandise, MarchandiseOperations, etc.) : reprendre le code qui fonctionne en y touchant le moins possible et le packager dans un bundle OSGi.
  * un client pour le bundle précédent sous forme de servlet (vous aurez donc besoin d'un container de servlets)

Packager sous forme de Web Application Bundle (WAB) - Aide : [http://coding.alasdair.info/2011/01/creating-web-application-bundle-using.html] et [http://www.javabeat.net/writing-an-osgi-web-application/] - et déployer dans OSGi. Tester avec l'application `tp1-web`.

Vous allez maintenant "sortir" les autres objets métiers du bundle Entrepot et les placer dans des bundles séparés. Pour chacun d'eux, créer un bundle exposant son interface et autant de bundles que d'implémentations de cette interface. Tester

### Implémentations multiples

Créez plusieurs implémentations de `Livraison` :
  * à pied
  * à vélo
  * par drone
  * en soucoupe volante

Chaque mode de livraison aura un délai de livraison (et un coût) différents.

**Indication** : issue de la page d'accueil d'OSGi, à propos du fait que vos livraisons implémentent la même interface.

> What happens when multiple bundles register objects under the same interface or class? How can these be distinguished? The answer is properties. Each service registration has a set of standard and custom properties. A expressive filter language is available to select only the services in which you are interested. Properties can be used to find the proper service or can play other roles at the application level.

Enregistrez ces bundles dans l'application et modifiez le client pour permettre à l'utilisateur de choisir son mode de livraison.

### Chargement / déchargement à chaud

Réalisez, pour au moins un object métier, plusieurs bundles correspondant aux différents types de DAO dont vous disposez. Ces bundles implémenteront l'interface commune de cet objet.

Démarrez le framework avec l'une de ces implémentations, interrogez le DAO avec le client, puis arrêtez le bundle en concerné et démarrez l'autre implémentation. Interrogez à nouveau le DAO et vérifiez que le client vous renvoie les données du nouveau DAO.

### Gestion des versions

Réaliser un autre composant qui implémente l'interface de l'entrepôt, et qui renvoie une réponse de type "service non disponible". Donner à ce composant un numéro de version inférieur à celui de l'entrepôt "fonctionnel". Modifier le fonctionnement de votre application pour qu'elle cherche l'entrepôt correspondant à une marchandise donnée et qu'il switche sur cette implémentation par défaut quand il ne trouve pas cette marchandise dans les entrepôts disponibles (par exemple, avec un OR dans le filtre de la propriété de la marchandise).

Simuler une panne en déployant en supprimant un service fonctionnel, sans arrêter le framework, et l'interroger entre temps avec le client. Vérifiez que le framework vous renvoie bien la dernière version du service demandé.

### Updates dynamiques

En vous abonnant aux événements du cycle de vie des bundles, mettez en place, dans le client, un système de notifications permettant à un utilisateur de savoir quand un entrepôt est ouvert ou fermé.
 
## Instructions de rendu

Ce TP est à rendre pour le dimanche 28 janvier 2018 à 23h59. Merci de remplir également le champ correspondant dans Tomuss.

package ipojo.ImplBundle;

import ipojo.InterfaceBundle.*;
import ipojo.InterfaceBundle.MarchandiseDAO;

public class MarchandiseOperationsImpl implements MarchandiseOperations {

    private MarchandiseDAO mdao;

    /**
     * Contruit un nouveau composant MarchandiseOperationsImpl.
     * @param mdao le DAO utilisé pour gérer la persistance des marchandises.
     */
    public MarchandiseOperationsImpl(MarchandiseDAO mdao) {
        this.mdao = mdao;
    }

    public void createOrUpdate(Marchandise marchandise) throws OperationFailedException {
        mdao.createOrUpdate(marchandise);
    }

    public Marchandise getByRef(int ref) {
        return mdao.findByRef(ref);
    }

    public void delete(Marchandise marchandise) throws OperationFailedException {
        mdao.remove(marchandise);
    }
}

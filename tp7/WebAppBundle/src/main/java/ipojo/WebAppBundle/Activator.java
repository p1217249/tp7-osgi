package ipojo.WebAppBundle;

import org.apache.felix.ipojo.annotations.Bind;
import org.apache.felix.ipojo.annotations.Requires;
import org.apache.felix.ipojo.annotations.Unbind;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import java.util.Hashtable;
import org.osgi.util.tracker.ServiceTracker;

import DictionaryService.DictionaryService;
import ipojo.APiedLivraison.LivraisonAPied;
import ipojo.AVeloLivrasion.LivraisonAVelo;
import ipojo.EnVolantLivraison.LivraisonEnVolant;
import ipojo.ParDroneLivraison.LivraisonParDrone;
public class Activator implements BundleActivator
{
	 // Bundle's context.
    private BundleContext m_context = null;
    // The service tacker object.
    private ServiceTracker m_tracker = null;
    
    @Requires(id="Apied")
    private LivraisonAPied apied_Livraison;
    @Bind(id="Apied")
    public void bindAPied() { System.out.println("Livrer a pied"); }
    @Unbind(id="Apied")
    public void unbindAPied() { System.out.println("arret a livrer a pied"); }

    @Requires(id="Avelo")
    private LivraisonAVelo Avelo_Livraison;
    @Bind(id="Avelo")
    public void bindAvelo() { System.out.println("Livrer a velo"); }
    @Unbind(id="Avelo")
    public void unbindAvelo() { System.out.println("arret a livrer a velo"); }
    
    @Requires(id="EnVolant")
    private LivraisonEnVolant EnVolant_Livraison;
    @Bind(id="EnVolant")
    public void bindEnVolant() { System.out.println("Livrer en volant"); }
    @Unbind(id="EnVolant")
    public void unbindEnVolant() { System.out.println("arret a livrer en volant"); }
    
    
    @Requires(id="ParDrone")
    private LivraisonParDrone ParDrone_Livraison;
    @Bind(id="ParDrone")
    public void bindParDrone() { System.out.println("Livrer par drone"); }
    @Unbind(id="ParDrone")
    public void unbindParDrone() { System.out.println("arret a livrer par drone"); }
    
    public void doSomething() { System.out.println("Faites le livraison"); }
    
    public void start(BundleContext context) throws Exception {
		System.out.println("Bundle " + context.getBundle().getSymbolicName() + " is starting ");
		Hashtable<String, String> props = new Hashtable<String, String>();
        props.put("Language", "Livraison");
        context.registerService(
            DictionaryService.class.getName(), new DictionaryImpl(), props);
    }

    /**
     * Implements BundleActivator.stop(). Does nothing since
     * the framework will automatically unregister any registered services.
     * @param context the framework context for the bundle.
    **/
    public void stop(BundleContext context) throws Exception {
    	System.out.println("Bundle " + context.getBundle().getSymbolicName() + " is stopping ");
    }
    
    private static class DictionaryImpl implements DictionaryService
    {
        // The set of words contained in the dictionary.
        String[] m_dictionary =
            { "Pied", "Velo", "Drone", "Volant" };

        /**
         * Implements DictionaryService.checkWord(). Determines
         * if the passed in word is contained in the dictionary.
         * @param word the word to be checked.
         * @return true if the word is in the dictionary,
         *         false otherwise.
        **/
        public boolean checkWord(String word)
        {
            word = word.toLowerCase();

            // This is very inefficient
            for (int i = 0; i < m_dictionary.length; i++)
            {
                if (m_dictionary[i].equals(word))
                {
                    return true;
                }
            }
            return false;
        }
    }

}

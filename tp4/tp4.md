# Clients de services

L'objectif de ce TP est d'appréhender les services Web du côté client.

Ce TP n'est pas noté.

## Application fournie

Le module `provided-services` contient une application Spring Boot qui expose un service SOAP ainsi qu'une ressource REST.
Ces services sont visibles depuis la page d'accueil de l'application.

Le service SOAP `magasin` contient deux opérations:
- Une opération d'approvisionnement de marchandise
- Une opération de commande d'un ensemble de marchandises.
  Cette opération peut échouer si une marchandise n'est pas présente en quantité suffisante.
  
La ressource REST représente les marchandises disponibles dans le magasin.

Le contenu du magasin est réinitialisé avec les données suivantes à chaque démarrage de l'application (les données ne sont pas sauvées sur disque)

## SOAPUI

[SOAPUI](https://www.soapui.org) est une application permettant de tester des services. 
Un miroir à l'université est disponible pour les différents systèmes ([Linux](https://box.univ-lyon1.fr/p/56bcab), [MacOS X](https://box.univ-lyon1.fr/p/cc78d9), [Windows](https://box.univ-lyon1.fr/p/be292b)).

## Test simples

Créer un nouveau projet dans SOAPUI.
Ajouter un nouveau WSDL (clic droit sur le projet) et indiquer l'adresse Wed du WSDL exposé par l'application Spring Boot.
Demander à créer des requêtes pour chacune des opérations.
Une fois le WSDL correctement ajouté, modifier puis exécuter ces requêtes.

Faire de même avec le [Swagger](https://swagger.io/) décrivant la ressource REST ([ici](http://localhost:8080/v2/api-docs)).
Les informations sont également disponibles à partir de l'[interface fournie par SpringFox](http://localhost:8080/swagger-ui.html).

## Suites de tests

Créer une nouvelle `TestSuite` dans le projet.
Y ajouter un `TestCase`.
Ajouter plusieurs requêtes dans cette suite de façon à ce qu'elles puissent s'enchaîner (par exemple un approvisionnement suffisant suivi d'une commande sur la marchandise approvisionnée).
Ajouter des étapes de vérification mettant en jeu la ressource REST pour vérifier que les marchandises ont été correctement ajoutées/supprimées.
Vérifier également le bon fonctionnement de la ressource REST.

## Client

Créer un client Web pour le service SOAP.


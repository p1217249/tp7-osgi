# TP5: Implémentation de services

L'objectif de ce TP est de comprendre les principes d'implémentation de Web services illustrés à travers la plateforme Java en utilisant le _framework_ [SpringBoot](https://projects.spring.io/spring-boot/) ainsi que le framework [CXF](http://cxf.apache.org/).

Il est demandé d'exposer les fonctionnalités de l'interface `EntrepotOperations` sous forme de service Web de type SOAP.
Il est possible de s'inspirer du sous-projet  `provided-services` pour comprendre comment configurer SpringBoot afin d'exposer un service SOAP.

Ce TP est à rendre pour le dimanche 17/12/2017 (c.f le [README](../README.md) pour les modalités).

Dans ce TP, on exposera de quatre manières différentes les fonctionnalités d'`EntrepotOperations` sous forme de service SOAP.
Le fichier [entrepot.wsdl](src/main/resources/entrepot.wsdl) contient les définitions du service.
Il s'appuie sur les fichiers [entrepots.xsd](src/main/resources/entrepots.xsd). 

## Implémentation via un générateur de code


### Génération

Utiliser le plugin cxf-codegen-plugin ([doc](http://cxf.apache.org/docs/maven-cxf-codegen-plugin-wsdl-to-java.html)) pour générer une interface Java représentant le service à implémenter.
 
>  Le code généré ne devra pas être versionné et se trouvera (par défaut) dans l'arborescence du répertoire target. 
>  Il est d'ailleurs conseillé de ne pas configurer de sourceRoot. 

Parcourir le code généré et faire le lien avec le contenu du fichier [entrepot.wsdl](src/main/resources/entrepot.wsdl).

### Implémentation via @WebService

Créer un composant Spring:

* qui implémente l'interface générée;
* annoté par `@WebService`;
* utilise une implémentation de `EntrepotOperations` pour l'implémentation du métier.

Déployer ce composant dans CXF.
On pourra s'inspirer du service exemple de `provided-services`.

>  Attention: bien remplir l'annotation `@WebService` en spécifiant de préférence à cet endroit l'emplacement du wsdl.

Tester le service déployé avec SOAPUI.

## Contrôleur de service



Le rôle de ce contrôleur est double: extraire le contenu XML dans des instances de classes Java et aiguiller la suite du traitement vers la bonne méthode du composant d'implémentation du service.

Créer un nouveau composant Spring implémentant l'interface [Provider<Source>](https://docs.oracle.com/javaee/7/api/index.html?javax/xml/ws/Provider.html). 
La méthode invoke devra convertir le XML du payload en objets Java et appeler la bonne méthode du composant d'implémentation du service que vous avez mis en place à l'étape précédente.

Déployer ce composant comme un Web service à une autre adresse et tester avec SOAPUI.


## Contrôleur principal (front)

 Implémenter un contrôleur point d'accès au service via un @Controller SpringMVC. 
 Ce contrôleur traitera les requêtes HTTP et extraira le contenu (payload) du message via l'API SAAJ. 
 Il redirigera ensuite le traitement vers le composant Web service provider de l'étape précédente.

Utiliser soapUI pour tester votre contrôleur point d'accès. 

> Pour fabriquer correctement les MimeHeaders, il faut les copier via une boucle depuis les headers de la requête HTTP. 

> En cas d'utilisation de l'API XML de transformation pour faire des copies entre représentations XML, penser à SAAJResult qui permet d'écrire dans noeud XML d'un message SOAP.